import basetranslateasyncadapter

import logging
from cerberus import Validator
from dotenv import load_dotenv
import os


from adapter_utils import get_translation_targets, get_supported_languages

# config
INTERNAL_ENDPOINT_BASEDOMAIN = "internal_endpoint_basedomain"

# payload params
INPUT =  "input"
SOURCE_LANGUAGE_ID = "source_language_id"
TARGET_LANGUAGE_IDS = "target_language_ids"
FORMAT = "format"

UNIQUE_HASH = "uniqueHash"
SOURCE_ID = "source_id"


PAYLOAD_SCHEMA = {
    INPUT: {'anyof': [{'type': 'dict', 'allow_unknown': True}, {'type': 'string'}], 'required': True},
    SOURCE_LANGUAGE_ID: {'type': 'string'},
    TARGET_LANGUAGE_IDS: {'type': 'list'},
    FORMAT: {'type': 'string', 'allowed': ['text', 'html', 'transcription']} # maybe use exclude with this
}

TRANSCRIBE_AND_TRANSLATE_COLLECTION = "transcribe_and_translate"

class Adapter(basetranslateasyncadapter.BaseTranslateAsyncAdapter):

    def __init__(self, connector, config_path):
        super(Adapter, self).__init__(connector, config_path)
        
        print('Generic Translate Adapter ... config:', self.config)
        
        self.supported_language_ids = get_supported_languages(self.config.get("generic_translate").get(INTERNAL_ENDPOINT_BASEDOMAIN).rstrip("/"))
        self.translation_target_topics = get_translation_targets(self.config.get("generic_translate").get(INTERNAL_ENDPOINT_BASEDOMAIN).rstrip("/"))
        self.peertube_collection = self.set_up_database()


    def run(self, payload, msg):
        print('This is the Generic Translate Adapter')
        print('Generic Translate Adapter ... run ... msg:', msg)
        result = {}
        success = False
        source_id = None
        try:
            if UNIQUE_HASH in payload:
                result[UNIQUE_HASH] = payload[UNIQUE_HASH]
            if SOURCE_ID in payload:
                source_id = payload[SOURCE_ID]
                result[SOURCE_ID] = payload[SOURCE_ID]

            validator = Validator(PAYLOAD_SCHEMA)
            validator.allow_unknown = True
            if validator.validate(payload) is False:
                # validator.errors could be for example {'input': ['required field'], 'format': ['unallowed value ee']}
                logging.error(f"validation error: {validator.errors}")
                result['success'] = False
                return result

            source_language_id = None
            if SOURCE_LANGUAGE_ID in payload:
                source_language_id = payload[SOURCE_LANGUAGE_ID]


            # TODO make sure the own language is not translated! but first add it on the peertube side to the db
            # target_language_ids.remove(source_language_id)
            # consider a variable that allows also returning the source language input in the payload to make it easier for programmers.
            target_language_ids = []
            if TARGET_LANGUAGE_IDS in payload:
                target_language_ids += payload[TARGET_LANGUAGE_IDS]
                if source_language_id in target_language_ids:
                    if not target_language_ids:
                        logging.info("target language ids list contains only source language id - nothing to translate")
                        return result
            else:
                if source_language_id:
                    for language_section in self.supported_language_ids:
                        if language_section["code"] == source_language_id:
                            target_language_ids += language_section["targets"]
                else:
                    # get the first one which should have all of them TODO does not yet work
                    if self.supported_language_ids[0]:
                       target_language_ids += self.supported_language_ids[0]["targets"]
            self.translation_result_success = False
            logging.info(f"source_language_id: {source_language_id}")
            logging.info(f"target_language_ids: {target_language_ids}")
            if(FORMAT in payload):
                # TODO switch case with a default returning an error this format is not supported and which are
                if payload[FORMAT] == "transcription":
                    translations = super().translate_transcription_xml(payload[INPUT], source_language_id, target_language_ids)
                if payload[FORMAT] == "html":
                    translations = super().translate_html(payload[INPUT], source_language_id, target_language_ids)
                if payload[FORMAT] == "text":
                    translations = super().translate_text(payload[INPUT], source_language_id, target_language_ids)
            else:
                translations = super().translate_text(payload[INPUT], source_language_id, target_language_ids)

            result["payload"] = translations
            result["type"] = "translations"
            if self.translation_result_success:
                success = True
            else:
                logging.error("translation not successful")

        except Exception: # probably best to remove this once it's stable enough and doesn't crash too often
            logging.exception("Exception thrown")

        if "response_topic_id" in payload:
            if "videoId" in payload:
                result["videoId"] = payload["videoId"]
            result["topic"] = payload["response_topic_id"]
            self.connector.sendResponseTo(result)

        result['success'] = success
        if source_id is not None:
           logging.info(f"source_id: {source_id}")
           logging.info(f"result before updating collection: {result}")

           doc = self.get_data_by_source_id(source_id)
           logging.info(f"doc: {doc}")
           doc["translations"] = result
           self.peertube_collection.update_one({'source_id':source_id}, {"$set": doc}, upsert=False)
           if "payload" in result:
               logging.info(f"payload: {result['payload']}")
               del result["payload"]
        else:
            logging.info(f"source_id is None")


        logging.info(f"result after updating collection: {result}")
        return result


    def set_up_database(self):
        load_dotenv() # do we need this, doesn't it happen anyway?
        
        config = {
            'username': os.getenv('MONGO_USERNAME'),
            'password': os.getenv('MONGO_PASSWORD'),
            'host': os.getenv('MONGO_HOST'),
            'database': os.getenv('MONGO_DATABASE'),
            'port': int(os.getenv('MONGO_PORT'))
        }
        db = self.connector.database['mongo'].MongoDB(config)
        db.connect()
        collection = db.get_collection(TRANSCRIBE_AND_TRANSLATE_COLLECTION)
        return collection

    def get_data_by_source_id(self, source_id):
        query = {"source_id": source_id}
        source_id_doc = list(self.peertube_collection.find(query))[0] # according to this - https://stackoverflow.com/a/62550243/9921564
        return source_id_doc

    def adapter_name(self):
        return 'generic_translate'