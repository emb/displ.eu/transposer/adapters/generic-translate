### Install Dependencies
You need the .env for the mongodb and add the right settings in the config.ini of the connector:

```ini
[database]
types = ["mongo"]

[mongo]
enable = true
host = 10.0.0.12
```





```bash
pip install kafka-python "pymongo[srv]" paramiko scp uuid deepmerge python-dateutil requests spacy beautifulsoup4

python -m spacy download ca_core_news_sm
python -m spacy download zh_core_web_sm
python -m spacy download hr_core_news_sm
python -m spacy download da_core_news_sm
python -m spacy download nl_core_news_sm
python -m spacy download en_core_web_sm
python -m spacy download fi_core_news_sm
python -m spacy download fr_core_news_sm
python -m spacy download de_core_news_sm
python -m spacy download el_core_news_sm
python -m spacy download it_core_news_sm
python -m spacy download ja_core_news_sm
python -m spacy download ko_core_news_sm
python -m spacy download lt_core_news_sm
python -m spacy download mk_core_news_sm
python -m spacy download xx_ent_wiki_sm
python -m spacy download nb_core_news_sm
python -m spacy download pl_core_news_sm
python -m spacy download pt_core_news_sm
python -m spacy download ro_core_news_sm
python -m spacy download ru_core_news_sm
python -m spacy download sl_core_news_sm
python -m spacy download es_core_news_sm
python -m spacy download sv_core_news_sm
python -m spacy download uk_core_news_sm

```
